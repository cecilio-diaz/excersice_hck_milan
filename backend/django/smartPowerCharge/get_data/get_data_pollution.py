import requests
import json
import time

def get_data_pollution(URL_DB):
    URL       = 'https://api.openaq.org/v1/measurements?limit=100'  # API-REST Data open-source

    data      = requests.get(URL)
    data      = data.json()

    meta      = data['meta']
    license2  = meta['license']
    website   = meta['website']
    page      = meta['page']
    limit     = meta['limit']
    results   = data['results']

    for i in results:
        resutls     = i
        location    = resutls['location']
        parameter   = resutls['parameter']
        date        = i['date']
        date_utc    = date['utc']
        date_local  = date['local']
        value       = i['value']
        coordinates = i['coordinates']

        latitude    = coordinates['latitude']
        longitude   = coordinates['longitude']
        country     = i['country']
        city        = i['city']

        print('| city %30s |,latitude %5.15f | ,longitude %5.15f, value %4.4f'%(city,latitude,longitude,value))

        obj_data= {
            "date_create": date_utc,
            "country": country,
            "city": city,
            "dataSource": "api.openaq.org",
            "latitude": latitude,
            "longitude":longitude,
            "status": False,
            "placesAvailable": 2,
            "placesOccupied": 4,
            "radius": 1
            }

        solicitud = requests.post(URL_DB, data = obj_data)
        code      = solicitud.status_code
        if code == 200 or code == 201 :
            A = 1
        elif code == 400:
            print('Incorrect request error')
        elif code == 500:
            print('Internal server error')

while True:
    try:
        URL_DB    = 'django_c3:8000/api/v1/localization_station'
        get_data_pollution(URL_DB)
        time.sleep(5)
    except:
        print('ERROR')
