from django.contrib               import admin
from django.urls                  import path
from django.conf                  import settings
from django.conf.urls             import url, include
from django.views.static          import serve
from django.urls                  import include, path
from rest_framework               import routers
from rest_framework.urlpatterns   import format_suffix_patterns

# VISTAS DE LAS APIS.
from apps.powerCharge.api.views import localization_Set

router = routers.DefaultRouter()
router.register('api/v1/localization_station',localization_Set)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include(router.urls)),
]
