# Generated by Django 2.2.3 on 2019-10-31 12:51

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='dato',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_create', models.CharField(default=django.utils.timezone.now, max_length=400, null=True)),
                ('date_update', models.FloatField(null=True)),
                ('country', models.CharField(default='italy', max_length=60, null=True)),
                ('city', models.CharField(default='milano', max_length=60, null=True)),
                ('dataSource', models.CharField(default='', max_length=60, null=True)),
                ('latitude', models.FloatField(default=45.515904, null=True)),
                ('longitude', models.FloatField(default=9.21039, null=True)),
            ],
        ),
    ]
