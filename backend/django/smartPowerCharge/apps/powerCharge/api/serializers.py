""" Maquinas EAM serializers """
# Modelos
# Danjo Rest Framework
from rest_framework              import serializers
from apps.powerCharge.models     import localization

class localization_Serializers(serializers.ModelSerializer):
    class Meta:
        model  = localization
        fields = (
                'id',
                'date_create',
                'country',
                'city',
                'dataSource',
                'latitude',
                'longitude',
                'status',
                'placesAvailable',
                'placesOccupied',
                'radius')
