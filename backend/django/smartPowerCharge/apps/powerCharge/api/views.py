from  django.shortcuts           import render
from rest_framework              import viewsets
from url_filter.integrations.drf import DjangoFilterBackend

from .serializers                import localization_Serializers
from apps.powerCharge.models     import localization

class localization_Set(viewsets.ModelViewSet):
    queryset         = localization.objects.all().order_by('-id')
    serializer_class = localization_Serializers
    filter_backends  = [DjangoFilterBackend,]
    filter_fields    = ['nombre','date_create','country','city','status','placesAvailable',]
