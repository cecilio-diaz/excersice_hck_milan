from django.apps import AppConfig


class PowerchargeConfig(AppConfig):
    name = 'powerCharge'
